# Fast API - Dummy

Dummy projecy with an API to explore FastAPI.

* Documentation: [https://fastapi.tiangolo.com/](https://fastapi.tiangolo.com/)

## Run project

Execute:
```shell
make run
```